# DIPC_queue

Script to submit jobs to the DIPC - Atlas queue system

## Submition Script

"Sumbit" is the general script in order to submit single or multiple jobs.
The same script is used for any queue system (fdr or edr)

## Installation

It is possible just to download the files, copy all of them in the submition folder, change the ownership and execute with ./submit.
However I personally recomend to follow some steps in order to be more usefull.

First you should create a "bin" folder (or whatever name you like) inside you DIPC - HOME directory.
Afterwards, export the PATH into you .bashrc file.

```bash
[ ! -d ~/bin ] && mkdir ~/bin
echo 'export PATH=$HOME/bin:$PATH' >> ~/.bashrc
```

Download the source code and copy the compressed folder inside previously created bin directory.
Type the following commands from your ~/bin folder.

```bash
tar -zxvf dipc_queue-xx.tar.gz
mv dipc_queue-xx/* ~/bin
chmod 700 ~/bin/*
```

## Usage

From now on each time you log in you will be able to execute submit command from any PATH.
The command will ask for a file (for single calculations) or directory (for multiple calculations) if not given as the first argument.

```bash
submit file

submit
Submition file or directory: 
```

The script will find the file or directory, export the origin PATH and stablish a new and empty SCRATCH PATH.
The input files will be copied to the scratch directory and executed.

After the calculation finished, the user needs to move to the personal SCRATCH direcory and execute hsync giving the scratch path as first argument.

```bash
cd /scratch/user
hsync $path_to_scratch_folder
```
